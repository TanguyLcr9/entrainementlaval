﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Rotate_Move : MonoBehaviour
{
    public GameObject child;
    [Range(1, 5)]
    public int value;
    // Start is called before the first frame update
    void Start()
    {
        child = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        child.transform.localPosition = new Vector3(value, child.transform.localPosition.y, 0);
        transform.Rotate(Vector3.up, Time.deltaTime*45);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;

public class Blorg : MonoBehaviour
{
    public int Score;
    public bool B_AreGrab = false;
    public Transform T_Target;
    public GameObject GO_DeathParicule;
    public GameObject GO_DeathEye;

    private void Start()
    {
        StartCoroutine(PlaySound());
        float scales = Random.Range(0.3f, 1.1f);
        transform.localScale = new Vector3(scales, scales, scales);
        if (scales >0.4)
        {
            Score = 5;
        }
        else if (scales > 0.7)
        {
            Score = 15;
        }else
        {
            Score =30;
        }
    }
    public void GrabeBlorg(Transform newtarget)
    {
        T_Target = newtarget;
        B_AreGrab = true;
        Death();
        //GetComponent<MoveToRandomPos>().Abort();
        //GetComponent<MoveToRandomPos>().enabled = false;
        //GetComponent<NavMeshAgent>().enabled = false;

        //transform.DOMove(T_Target.position, 0.5f);
        //transform.SetParent(T_Target);
    }

    public void Death()
    {
        GameManager.instance.AddScore(Score);
        Instantiate(GO_DeathParicule, transform.position, Quaternion.identity);
        Instantiate(GO_DeathEye, transform.position, Quaternion.identity);
        GameManager.instance.I_NmbOfBlorgs--;
        Destroy(this.gameObject);
    }

    IEnumerator PlaySound()
    {
        yield return new WaitForSeconds(Random.Range(0,2));
        GetComponent<AudioSource>().Play();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlorgManager : MonoBehaviour
{
    public int I_NmbOfBlorgSpawn;
    public GameObject area;
    public GameObject GO_Blorg;
    public float F_rangeMin, F_rangeMax;
    private new Vector3 spawn;

    private void Start()
    {
        if(F_rangeMin > F_rangeMax) { return; }

        for (int i = 0; i < I_NmbOfBlorgSpawn; i++)
        {
            Spawning();
        }

        StartCoroutine(DoSpawn());
    }

    private Vector3 Spawn()
    {
        Vector3 newVector = Vector3.zero;

        for (int i = 0; i <= 50; i++)
        {
            Vector3 ranVector = new Vector3(Random.Range(-F_rangeMax, F_rangeMax), 0, Random.Range(-F_rangeMax, F_rangeMax));

            if (F_rangeMax > Vector3.Distance(ranVector, Vector3.zero) && Vector3.Distance(ranVector, Vector3.zero) > F_rangeMin){
                newVector = ranVector;
                break;
            }
        }

        return newVector;
       
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 1, 0, 0.25F);
        Gizmos.DrawSphere(transform.position, F_rangeMin);

        Gizmos.color = new Color(1, 0, 0, 0.25F);
        Gizmos.DrawSphere(transform.position, F_rangeMax);
    }

    public void Spawning()
    {
        spawn = Spawn();
        // Debug.Log(spawn);
        GameObject Blorgs = Instantiate(GO_Blorg, spawn, Quaternion.identity);
        Blorgs.GetComponent<MoveToRandomPos>().area = area;
        Blorgs.GetComponent<MoveToRandomPos>().minDist = F_rangeMin;
        Blorgs.GetComponent<MoveToRandomPos>().maxDist = F_rangeMax;
        GameManager.instance.I_NmbOfBlorgs++;

    }

    IEnumerator DoSpawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);

            Spawning();
        }
    }
}


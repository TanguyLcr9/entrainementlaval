﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI TMPU_ScoreDisplay;
    public Slider Slid_TimeDisplay;
    public int I_Score;
    public int I_Time;
    private float F_CurentTime;
    public int I_NmbOfBlorgs;
    public static GameManager instance;
    public List<Renderer> GO_Recipient;
    private float F_Amount;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        foreach(Renderer machin in GO_Recipient)
        {
            machin.material.SetFloat("_FillAmount", 1.6f);
        }
        F_CurentTime = I_Time;
        Slid_TimeDisplay.maxValue = I_Time;
        TMPU_ScoreDisplay.text = "Score : " + I_Score;
    }

    // Update is called once per frame
    void Update()
    {
        Slid_TimeDisplay.value = F_CurentTime;
        if (F_CurentTime > 0)
        {
            F_CurentTime -= Time.deltaTime;
        }
        else
        {
            EndGame();
        }

        if (I_NmbOfBlorgs == 0)
        {
            EndGame();
        }

    }

    public void EndGame()
    {

    }

    public float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public void AddScore(int addScore)
    {
        I_Score += addScore;
        float mapValue = Remap(I_Score, 0, 200, 1.6f, -0.65f);
        foreach (Renderer machin in GO_Recipient)
        {
            machin.material.SetFloat("_FillAmount", mapValue);
        }
        TMPU_ScoreDisplay.text = "Score : " + I_Score;
    }
}

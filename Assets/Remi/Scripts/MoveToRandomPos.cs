﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToRandomPos : MonoBehaviour
{
    private UnityEngine.AI.NavMeshAgent navAgent;
    public GameObject area;
    public float minDist, maxDist;
    public void Start()
    {
        navAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (navAgent == null)
        {
            Debug.LogWarning("The " + gameObject.name + " game object does not have a Nav Mesh Agent component to navigate. One with default values has been added", gameObject);
            navAgent = gameObject.AddComponent<UnityEngine.AI.NavMeshAgent>();
        }
        navAgent.SetDestination(getRandomPosition());

#if UNITY_5_6_OR_NEWER
        navAgent.isStopped = false;
#else
                navAgent.Resume();
#endif
    }

    private void Update()
    {
        if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance)
            navAgent.SetDestination(getRandomPosition());
    }

    private Vector3 getRandomPosition()
    {
        Vector3 newVector = Vector3.zero;

        for (int i = 0; i <= 50; i++)
        {
            Vector3 ranVector = new Vector3(Random.Range(-maxDist, maxDist), 0, Random.Range(-maxDist, maxDist));

            if (maxDist > Vector3.Distance(ranVector, Vector3.zero) && Vector3.Distance(ranVector, Vector3.zero) > minDist)
            {
                newVector = ranVector;
                break;
            }
        }

        return newVector;

        /* BoxCollider boxCollider = area != null ? area.GetComponent<BoxCollider>() : null;
         if (boxCollider != null)
         {
             return new Vector3(UnityEngine.Random.Range(area.transform.position.x - area.transform.localScale.x * boxCollider.size.x * 0.5f,
                                                         area.transform.position.x + area.transform.localScale.x * boxCollider.size.x * 0.5f),
                                area.transform.position.y,
                                UnityEngine.Random.Range(area.transform.position.z - area.transform.localScale.z * boxCollider.size.z * 0.5f,
                                                         area.transform.position.z + area.transform.localScale.z * boxCollider.size.z * 0.5f));
         }
         else
         {
             SphereCollider sphereCollider = area != null ? area.GetComponent<SphereCollider>() : null;
             if (sphereCollider != null)
             {
                 float distance = UnityEngine.Random.Range(-sphereCollider.radius, area.transform.localScale.x * sphereCollider.radius);
                 float angle = UnityEngine.Random.Range(0, 2 * Mathf.PI);
                 return new Vector3(area.transform.position.x + distance * Mathf.Cos(angle),
                                    area.transform.position.y,
                                    area.transform.position.z + distance * Mathf.Sin(angle));
             }
             else
             {
                 return gameObject.transform.position + new Vector3(UnityEngine.Random.Range(-5f, 5f), 0, UnityEngine.Random.Range(-5f, 5f));
             }
         }*/
    }

    public void Abort()
    {
#if UNITY_5_6_OR_NEWER
        navAgent.isStopped = true;
#else
                navAgent.Stop();
#endif
    }

    public void Play()
    {
#if UNITY_5_6_OR_NEWER
        navAgent.isStopped = false;
#else
                navAgent.Stop();
#endif
    }
}

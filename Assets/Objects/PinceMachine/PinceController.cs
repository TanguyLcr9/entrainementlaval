﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using DG.Tweening;
public class PinceController : MonoBehaviour
{
    public int mode;
    public SteamVR_Action_Vector2 moveAction = SteamVR_Input.GetAction<SteamVR_Action_Vector2>("platformer", "Move");
    public SteamVR_Action_Boolean grabAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("platformer", "Jump");
    private SteamVR_Input_Sources hand;
    private Interactable interactable;
    private Vector3 movement;
    private bool grab;

    public PinceBehaviour pince;
    // Start is called before the first frame update
    void Start()
    {
        interactable = GetComponent<Interactable>();
    }

    // Update is called once per frame
    void Update()
    {

        if (interactable.attachedToHand)
        {

            hand = interactable.attachedToHand.handType;

            if (mode == 1)
            {
                Vector2 m = moveAction[hand].axis;
                pince.Move(m.y, m.x);
            }
            else
            {

                grab = grabAction[hand].stateDown;
                pince.Grab(grab);
            }
        }
            

        
    }

}

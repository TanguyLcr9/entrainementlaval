﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PinceBehaviour : MonoBehaviour
{
    public Transform Grabber;
    public Transform GrabbingPoint;
    public float minClamp = 1, maxClamp = 4;

    private bool grabbing;
    public float speedMove =1f, speedRotation = 1f;

    public Tween grabingTween;
    public void Move(float movement, float rotation)
    {
        if (!grabbing)
        {
            Grabber.localPosition += new Vector3(movement * speedMove * Time.deltaTime, 0, 0);
            Grabber.localPosition = new Vector3(
            Mathf.Clamp(Grabber.localPosition.x, minClamp, maxClamp),
            Grabber.localPosition.y,
            Grabber.localPosition.z);

            transform.Rotate(Vector3.up, rotation * speedRotation * Time.deltaTime);
        }
    }

    public void Grab(bool grab)
    {
        if(!grabbing)
        grabbing = grab;

        if (grabbing && grabingTween == null)
        {
            grabbing = true;
            StartCoroutine(DoGrab());
            return;
        }
    }

    IEnumerator DoGrab()
    {
        grabingTween = Grabber.DOMove(GrabbingPoint.position,1f).SetLoops(2,LoopType.Yoyo);
        yield return grabingTween.WaitForElapsedLoops(1);
        GetComponent<Animator>().SetTrigger("Grab");
        yield return grabingTween.WaitForCompletion();
        grabingTween = null;
        grabbing = false;
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0, 1, 0, 0.25F);
        Gizmos.DrawSphere(transform.position, minClamp);

        Gizmos.color = new Color(1, 0, 0, 0.25F);
        Gizmos.DrawSphere(transform.position, maxClamp);
    }
}